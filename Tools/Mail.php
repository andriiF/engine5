<?php

namespace Engine5\Tools;

/**
 * Description of Mail
 *
 * @author barcis
 */
class Mail {

    /**
     * @var PHPMailer
     */
    private $provider = null;

    /**
     * @var array
     */
    private $to = array();
    private $replyToEmail = '';
    private $fromName = '';

    /**
     * @var \Engine\Config\Parser\Yaml
     */
    private $config;

    public function __construct($type = 'default') {

        $this->config = new \Engine5\Core\Engine\Config\Parser\Yaml('mail.config.yml', $type);


        $this->provider = new \PHPMailer();
        if ($this->config->smtp) {
            $this->provider->IsSMTP();
        } else {
            $this->provider->isSendmail();
        }
        $this->provider->SMTPAuth = true;
        $this->provider->Port = $this->config->port;
        $this->provider->SMTPSecure = $this->config->SMTPSecure;
        $this->provider->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $this->provider->CharSet = "UTF-8";
        $this->provider->Helo = $this->config->server;
        $this->provider->Host = $this->config->server;
        $this->provider->Username = $this->config->username;
        $this->provider->Password = $this->config->password;
        $this->provider->From = isset($this->config->from) ? $this->config->from : $this->config->username;
//        $this->provider->SMTPDebug = 1;
    }

    public function addTo($email, $name = '') {
        $this->to[] = array('email' => $email, 'name' => $name);
    }

    public function addStringAttachment($string, $filename, $type) {
        $this->provider->addStringAttachment($string, $filename, 'base64', $type);
    }

    public function setSubject($subject) {
        $this->provider->Subject = $subject;
    }

    public function setFromName($fromName) {
        $this->fromName = $fromName;
    }

    public function setReplyToEmail($replyToEmail) {
        $this->replyToEmail = $replyToEmail;
    }

    public function setContent($content) {
        $this->provider->AltBody = strip_tags($content);
        $this->provider->MsgHTML($content);
        $this->provider->IsHTML(true);
    }

    public function errorInfo() {
        return $this->provider->ErrorInfo;
    }

    public function send() {
        if ($this->replyToEmail != '') {
            $this->provider->AddReplyTo($this->replyToEmail, $this->fromName);
        } else {
            $this->provider->AddReplyTo($this->config->replyToEmail, $this->config->replyToName);
        }

        $this->provider->FromName = ($this->fromName != '') ? $this->fromName : $this->config->fromName;

        foreach ($this->to as $receiver) {
            $this->provider->AddAddress($receiver['email'], $receiver['name']);
        }
        return (bool) $this->provider->Send();
    }

}
