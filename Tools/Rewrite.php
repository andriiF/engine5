<?php

namespace Engine5\Tools;

class Rewrite {

    public static function create($string) {

        $lower = strtolower($string);
        $nopl = strtr($lower, [
            'ą' => 'a',
            'ć' => 'c',
            'ę' => 'e',
            'ł' => 'l',
            'ś' => 's',
            'ń' => 'n',
            'ó' => 'o',
            'ż' => 'z',
            'ź' => 'z',
            'Ą' => 'a',
            'Ć' => 'c',
            'Ę' => 'e',
            'Ł' => 'l',
            'Ś' => 's',
            'Ń' => 'n',
            'Ó' => 'o',
            'Ż' => 'z',
            'Ź' => 'z'
        ]);
        $unique = preg_replace('/[^a-z0-9]/', '-', $nopl);
        $unique = preg_replace('/\-\-+/', '-', $unique);
        $unique = trim($unique, '-');

        return $unique;
    }

}
