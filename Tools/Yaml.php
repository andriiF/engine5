<?php

namespace Engine5\Tools;

use Symfony\Component\Yaml\Parser;

/**
 * Description of Yaml
 *
 * @author barcis
 */
class Yaml {

    public static function parseFile($filename) {
        if (function_exists('yaml_parse_file')) {
            return yaml_parse_file($filename);
        } else {
            $yaml = new Parser();
            return $yaml->parse(file_get_contents($filename));
        }
    }

}
