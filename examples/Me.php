<?php

namespace Squad\Admin\API;

/**
 * Description of Me
 *
 * @author barcis
 */
class Me extends \Engine5\Core\Rest\Api {

    public function getOne() {
        $data = \Engine5\Core\Auth::getLoggedUser(true);
        unset($data->password);
        return new \Engine5\Core\Rest\Result($data->asArray(), 201);
    }

}
