<?php

namespace Engine5\Helper;

trait Response {

    public static function redirect($target, $httpCode = 301, array $params = array(), $append = '') {
        if (substr($target, 0, 7) !== 'http://') {
            $target = \Engine5\Tools\Link::url($target, $params);
        }
        \Engine5\Core\Engine::destroy();
        header('Debug: ' . $target . $append . ' Code: ' . $httpCode);
        header('Location: ' . $target . $append, true, $httpCode);
        exit();
    }

}
