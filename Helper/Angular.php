<?php

namespace Engine5\Helper;

trait Angular {

    private function loadAngularAppParts($path) {
        $fullPath = ST_APPDIR . '/frontend/app/' . $path . '/';
        if (file_exists($fullPath)) {
            $dir = dir($fullPath);

            while ($filename = $dir->read()) {
                $file = $fullPath . $filename;
                if (is_file($file) && preg_match('/\.js$/', $filename)) {
                    $fn = preg_replace('/\.js$/', '', $filename);
                    $this->app($path . '/' . $fn, 40);
                }
            }
        }
    }

    public function loadAngularApp() {
        $this->app('app', 20);
        $this->app('configs', 21);
        $this->app('helpers', 25);
        $this->app('controllers', 30);
        $this->app('router', 35);

        $this->loadAngularAppParts('classes');
        $this->loadAngularAppParts('controllers');
        $this->loadAngularAppParts('directives');
        $this->loadAngularAppParts('langs');
        $this->loadAngularAppParts('factories');
        $this->loadAngularAppParts('services');
        $this->loadAngularAppParts('filters');
        $this->loadAngularAppParts('models');
        $this->loadAngularAppParts('values');


        $AppName = \Engine5\Core\Engine::getCurrentAppName();
        $AppConfig = \Engine5\Core\Engine::getConfig()->configs[$AppName];

        if (isset($AppConfig['plugins'])) {
            foreach ($AppConfig['plugins'] as $plugin) {
                $rc = new \ReflectionClass($plugin['class']);

                if ($rc->hasMethod('scripts')) {
                    $scripts = $rc->getMethod('scripts')->invoke(null);
                    if (is_array($scripts)) {
                        foreach ($scripts as $script) {
                            $this->js($script, 40);
                        }
                    }
                }
            }
        }
    }

}
