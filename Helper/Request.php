<?php

namespace Engine5\Helper;

trait Request {

    private function prepareParams(&$limit, &$page, &$revers, &$sort, &$search, $default = null) {
        $this->prepareParamsSearch($search);
        $this->prepareParamsSort($revers, $sort, $default);
        $this->prepareParamsLimit($limit, $page);
    }

    private function prepareParamsId(&$id) {
        $id = isset($id) ? (int) $id : null;
    }

    private function prepareParamsSearch(&$search) {
        $search = isset($search) ? trim(urldecode($search)) : '';
    }

    private function prepareParamsSort(&$revers, &$sort, $default = null) {

        if (!$default) {
            $default = 'id';
        }

        $revers = ($revers == 'true' ? 'desc' : 'asc');
        $sort = isset($sort) ? $sort : $default;
    }

    private function prepareParamsLimit(&$limit, &$page) {
        $limit = (int) $limit;
        $page = (int) $page;
    }

}
