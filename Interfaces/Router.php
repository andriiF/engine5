<?php

namespace Engine5\Interfaces;

/**
 * Description of Factory
 *
 * @author barcis
 */
interface Router {

    function getBaseForAplication($app = null);

    /**
     * @param string $url
     * @return \Engine5\Core\ControllerViewArgs
     */
    function translateFromUrl($url, $method = null, $app = null, $uri = null);

    /**
     * @param \Engine5\Core\ControllerViewArgs $controllerViewArgs
     * @return string
     */
    function translateToUrl(\Engine5\Core\ControllerViewArgs $controllerViewArgs);
}
