<?php

namespace Engine5\Interfaces\Form\Field;

/**
 *
 * @author barcis
 */
interface Action {

    public function setAction(callable $action);

    public function getAction();
}
