<?php

namespace Engine5\Interfaces;

/**
 * Description of Controller
 *
 * @author barcis
 */interface Container {

    /**
     * @return \Engine5\Core\Placeholder;
     */
    function getPlaceHolder($name);
}
