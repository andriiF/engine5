<?php

namespace Engine5\Interfaces\Templater\Smarty;

/**
 * Description of Session
 *
 * @author barcis
 */
interface Plugin {

    static function register(\Smarty $smarty);
}
