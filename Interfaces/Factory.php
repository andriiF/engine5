<?php

namespace Engine5\Interfaces;

/**
 * Description of Factory
 *
 * @author barcis
 */
interface Factory {

    static function newInstance(array $args = array());

    static function newSpecyficInstance($type, array $args = array());
}
