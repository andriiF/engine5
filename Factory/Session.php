<?php

namespace Engine5\Factory;

/**
 * Description of Session
 *
 * @author barcis
 */
class Session implements \Engine5\Interfaces\Factory {

    /**
     * @return \Engine5\Interfaces\Factory
     */
    public static function newInstance(array $args = array()) {
        return self::newSpecyficInstance('\\Engine5\\Core\\Session\\Standard', $args);
    }

    /**
     * @return \Engine5\Interfaces\Factory
     */
    public static function newSpecyficInstance($type, array $args = array()) {
        $rfc = new \ReflectionClass($type);
        return $rfc->getMethod('getInstance')->invoke(null);
    }

}
