<?php

namespace Engine5\Factory;

/**
 * Description of session
 *
 * @author barcis
 */
class Router implements \Engine5\Interfaces\Factory {

    /**
     * @var Router[]
     */
    private static $routers = array();

    /**
     * @param array $args
     * @return \Engine5\Interfaces\Router
     */
    public static function newInstance(array $args = array(), $type = null) {

        if ($type === null) {
            $type = (\Engine5\Core\Engine::getEntryPoint() === \Engine5\Core\Engine\Entrypoint::REST ? 'Rest' : 'Standard');
        }
        return self::newSpecyficInstance($type, $args);
    }

    /**
     *
     * @param string $type
     * @param array $args
     * @return \Engine5\Interfaces\Router
     */
    public static function newSpecyficInstance($type, array $args = array()) {
        $class = '\\Engine5\\Core\\Router\\' . $type;
        if (!isset(self::$routers[$class])) {
            $rfc = new \ReflectionClass($class);
            self::$routers[$class] = $rfc->newInstanceArgs($args);
        }
        return self::$routers[$class];
    }

}
