<div id="engin5-debug-box">
    <div class="dg-top">
        <div class="dg-logos"></div>
        <div class="dg-content">
            <div>dbConf: <strong>[{$this->app->config.database.config.default}]</strong></div>
            <div>time: <strong>[{$this->app->execution_time|round:2}]</strong></div>
            <div>life: <strong>[{$this->app->maxlifetime|round:1}]m ([{($this->app->maxlifetime/60)|round:1}]h)</strong></div>
        </div>
        <hr/>
    </div>
    <div class="dg-content-more"></div>
</div>
<style type="text/css">
    #engin5-debug-box {
        position: fixed;
        top: 1px;
        right: 30px;
        background-color: #003647;
        color: #FDFDFD;
        outline: 2px solid #003647;
        border: 1px solid #FDFDFD;
        height: 37px;
        width: 200px;
        overflow: hidden;
        z-index: 9999;
    }
    #engin5-debug-box.open {
        height: 400px;
        width: 400px;
        z-index: 9999;
    }
    #engin5-debug-box hr {
        clear: both;
        margin: 0;
    }
    #engin5-debug-box .dg-content-more {
        font-size: 10px;
        line-height: 10px;
        padding: 5px;
        overflow: auto;
        height: 361px;
    }
    #engin5-debug-box .dg-content {
        font-size: 9px;
        line-height: 9px;
        padding: 5px;
        float: left;
        max-height: 36px;
        overflow: hidden;
    }
    #engin5-debug-box .dg-logos {
        float: left;
    }
    #engin5-debug-box .dg-logos:after {
        height: 13px;
        width: 50px;
        background-repeat: no-repeat;
        background-position: center center;
        background-image: url('https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/Mar/19/engine5-logo-861128654-1_avatar.png');
        background-size: 50px 50px;
        content: ' ';
        display: block;
    }
    #engin5-debug-box .dg-logos:before {
        height: 22px;
        width: 50px;
        background-repeat: no-repeat;
        background-position: center center;
        background-image: url('https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2013/Nov/19/barcis-avatar-897863784-2.png');
        background-size: 50px 50px;
        content: ' ';
        display: block;
    }
</style>
<script>
    var $$debuger = document.$$debuger = $('#engin5-debug-box');
    $$debuger.init = function () {

        if ((typeof app) !== 'undefined') {
            app.config(function ($httpProvider) {
                $httpProvider.interceptors.push(function ($q) {
                    return {
                        'responseError': function (rejection) {
                            if (rejection.status === 409 && parseInt(rejection.headers('engin5-debug-status')) === 500) {
                                if ($$debuger) {
                                    $$debuger.appendFromDD(rejection);
                                    $$debuger.openDebuger();
                                } else {
                                    console.log('dd:', rejection.data.arguments);
                                }
                            }
                            return $q.reject(rejection);
                        }
                    };
                });
            })
        }


        this.container = $$debuger.find('.dg-content-more');

        function createCookie(name, value, days) {
            var expires;

            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "";
            }
            document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = encodeURIComponent(name) + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0)
                    return decodeURIComponent(c.substring(nameEQ.length, c.length));
            }
            return null;
        }

        var debug = readCookie('engine5-debuger');

        if (debug === null) {
            createCookie('engine5-debuger', 'on');
        } else if (debug === 'off') {
            $$debuger.hide('fast');
            console.info('Press "Ctrl + Q" to open debbuger.');
        }

        this.showDebuger = function () {
            $$debuger.fadeIn('fast');
            createCookie('engine5-debuger', 'on');
        };
        this.hideDebuger = function () {
            $$debuger.fadeOut('fast');
            createCookie('engine5-debuger', 'off');
            this.closeDebuger();
        };
        this.openDebuger = function () {
            $$debuger.showDebuger();
            $$debuger.addClass('open');
        };
        this.closeDebuger = function () {
            $$debuger.removeClass('open');
        };

        this.toggleFullDebuger = function () {
            $$debuger.toggleClass('open');
        };
        this.toggleDebuger = function ()
        {
            if ($$debuger.is(':visible')) {
                $$debuger.hideDebuger();
            } else {
                $$debuger.showDebuger();
            }
        };
        this.appendFromDD = function (ddData) {
            this
                    .container
                    .append('<div class="dbg-row">' +
                            '<div>url: (' + ddData.config.method + ')' + ddData.config.url + ' status: ' + ddData.status + '</div>' +
                            '<div>' + JSON.stringify(ddData.data.arguments) + '</div>' +
                            '</div>');
        };

        $(document.body).on("keydown", function (event) {
            if (!event.altKey && event.ctrlKey && event.which === 81) {
                $$debuger.toggleDebuger();
                event.stopPropagation();
            } else if (event.altKey && event.ctrlKey && event.which === 81) {
                if ($$debuger.hasClass('open')) {
                    $$debuger.closeDebuger();
                } else {
                    $$debuger.openDebuger();
                }
            }
        });

        $$debuger.find('.dg-top').on('click', function () {
            $$debuger.toggleFullDebuger();
        });
    };
    $$debuger.init();

</script>