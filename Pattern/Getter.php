<?php

namespace Engine5\Pattern;

/**
 * Description of Getter
 *
 * Klasa abstrakcyjna. Zapewnia interfejs typu magic method __get($key)
 *
 * @author barcis
 */
abstract class Getter {

    /**
     * @var array
     */
    private $_data;

    protected function __construct(array $data) {
        $this->setData($data);
    }

    final protected function setData(array $data) {
        $this->_data = $data;
    }

    final public function __isset($name) {
        return isset($this->_data[$name]);
    }

    public function __get($name) {
        if (isset($this->_data[$name])) {
            return $this->_data[$name];
        }
        throw new \Engine5\Exception\Getter("Podany index '{$name}' nie istnieje!");
    }

}
