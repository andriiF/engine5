<?php

namespace Engine5\Pattern;

/**
 * Description of EP_list
 *
 * @author barcis
 */
class Lists implements \ArrayAccess, \Iterator {

    /**
     * @var array
     */
    protected $items = array();
    private $position = 0;

    public function insert($key, $value, $position) {
        $this->items = array_merge(array_slice($this->items, 0, $position), array($key => $value), array_slice($this->items, $position));
    }

    public function append($value) {
        $this->items[] = $value;
    }

    public function offsetExists($offset) {
        return isset($this->items[$offset]);
    }

    public function offsetGet($offset) {
        if (!isset($this->items[$offset])) {
            return null;
        }
        return $this->items[$offset];
    }

    public function offsetSet($offset, $value) {
        if (@strlen($offset) == 0) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetUnset($offset) {
        unset($this->items[$offset]);
    }

    public function count() {
        return count($this->items);
    }

    public function isEmpty() {
        return (bool) ($this->count() == 0);
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        $tmp = array_keys($this->items);
        if (isset($tmp[$this->position]) && isset($this->items[$tmp[$this->position]])) {
            return $this->items[$tmp[$this->position]];
        }
        return null;
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        $this->position++;
    }

    public function valid() {
        $tmp = array_keys($this->items);
        return isset($tmp[$this->position]);
    }

    public function getItems() {
        return $this->items;
    }

    public function asArray() {
        $return = array();
        foreach ($this->items as $item) {
            if (is_object($item) && method_exists($item, 'asArray')) {
                $return[] = $item->asArray();
            } else {
                $return[] = $item;
            }
        }
        return $return;
    }

}
