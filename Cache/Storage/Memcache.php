<?php

namespace Engine5\Cache\Storage;

/**
 * Description of Memcache
 *
 * @author barcis
 */
class Memcache implements \Engine5\Cache\Storage {

    private $name;
    private $basedir;
    private $dir;
    private $timeout;

    public function __construct($name, $config) {

    }

    public function isEnabled() {
        return $this->enabled;
    }

    public function get($id, $timeout = null) {
        $filename = $this->getFileName($id);

        if (file_exists($filename) && (time() - filectime($filename) < $this->timeout)) {
            return unserialize(file_get_contents($filename));
        } else {
            throw new \Engine5\Cache\CacheExpireException('Cache ' . $filename . ' expired');
        }
    }

    public function getConfig($id, $timeout = null) {

    }

    public function set($id, $data, $timeout = null) {
        $filename = $this->getFileName($id);
        file_put_contents($filename, $data);
    }

}
