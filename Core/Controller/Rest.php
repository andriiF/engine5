<?php

namespace Engine5\Core\Controller;

use \Engine5\Core\Engine;

class Rest extends \Engine5\Core\Controller {

    private function getMethodAndParamsNames($action) {
        preg_match('/^(?<method>[a-zA-Z1-9_]+)\ *(\((?<params>.*)\))?/', $action, $m);
        if (isset($m['params'])) {
            $m['params'] = explode(',', preg_replace('/\s/', '', $m['params']));
        } else {
            $m['params'] = array();
        }
        return array($m['method'], $m['params']);
    }

    public function __init() {
        $cva = Engine::getCurrentApp()->getControllerViewArgs();
        list($apiClass, $action) = explode('::', $cva->action);
        list($method, $paramsNames) = self::getMethodAndParamsNames($action);

        $requestMethod = Engine::getRequestMethod();
        $params = array();

        $srcParams = $this->params;

        if (!isset($srcParams[$requestMethod])) {
            $srcParams[$requestMethod] = array();
        }

        foreach ($paramsNames as $param) {
            if (isset($srcParams[$param])) {
                $params[] = $srcParams[$param];
            } elseif (is_array($srcParams[$requestMethod]) && isset($srcParams[$requestMethod][$param])) {
                $params[] = $srcParams[$requestMethod][$param];
            } elseif (is_object($srcParams[$requestMethod]) && isset($srcParams[$requestMethod]->{$param})) {
                $params[] = $srcParams[$requestMethod]->{$param};
            } elseif (is_array($srcParams['get']) && isset($srcParams['get'][$param])) {
                $params[] = $srcParams['get'][$param];
            } else {
                $params[] = null;
            }
        }




        $rfc = new \ReflectionClass($apiClass);

        if (!$rfc->isSubclassOf('\Engine5\Core\Rest\Api')) {
            http_response_code(404);
            echo "Klasa {$apiClass} nie rozszerza \Engine5\Core\Rest\Api!";
            return;
        }

        $api = $rfc->newInstance($srcParams, $srcParams[$requestMethod]);

        if (!method_exists($api, $method)) {
            http_response_code(404);
            echo "Metoda {$apiClass}::{$method} nie istnieje!";
            return;
        }
        $cacheConfig = false;

        if ($this->getCacheEnabled()) {
            if ($this->getCacheIncludeLogin()) {
                $cachekey = md5(serialize([$apiClass, $method, $params, \Engine5\Core\Auth::isLogged(), \Engine5\Core\Auth::getLoggedUserId()]));
            } else {
                $cachekey = md5(serialize([$apiClass, $method, $params]));
            }

            try {
                $return = \Engine5\Cache\Core::instance()->get('api', $cachekey, $this->getCacheTimeout());
                $cacheConfig = \Engine5\Cache\Core::instance()->getConfig('api', $cachekey, $this->getCacheTimeout());
            } catch (\Engine5\Cache\CacheExpireException $ex) {
                try {
                    $return = call_user_func_array(array($api, $method), $params);
                    \Engine5\Cache\Core::instance()->set('api', $cachekey, $return, $this->getCacheTimeout());
                    $cacheConfig = [
                        'cached' => false,
                        'timeout' => $this->getCacheTimeout(),
                        'created' => time(),
                        'expires' => time() + $this->getCacheTimeout()
                    ];
                } catch (\Exception $exc) {
                    $cacheConfig = false;
                    http_response_code(409);

                    $text = nl2br($exc->__toString());

                    echo "E5REST EXCEPTION:<br />\n{$text}";
                    return;
                }
            }
        } else {
            try {
                $return = call_user_func_array(array($api, $method), $params);
            } catch (\Exception $exc) {
                http_response_code(409);

                $text = nl2br($exc->__toString());

                echo "E5REST EXCEPTION:<br />\n{$text}";
                return;
            }
        }

        if ($return instanceof \Engine5\Core\Rest\Result) {

            $return->sendCode($cacheConfig, $this->getCacheEnabled());
            echo $return->getJSON();
        } else {
            http_response_code(404);
            echo "Metoda zwróciła niepoprawny typ danych!";
            return;
        }
    }

}
