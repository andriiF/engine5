<?php

namespace Engine5\Core;

use Engine5\Core\Engine\Entrypoint;

/**
 * @property string $name
 * @property integer $begin
 * @property integer $end
 * @property integer $execution
 * @property boolean $isRunning
 */
class DebugExecutionTime {

    private $name;
    private $begin;
    private $end;
    private $running = false;

    private function getmicrotime() {
        list($usec, $sec) = explode(' ', microtime());
        return ((float) $usec + (float) $sec);
    }

    public function __construct($name) {
        $this->name = $this->name;
        $this->begin = $this->getmicrotime();
        $this->running = true;
    }

    public function stamp() {
        $this->end = $this->getmicrotime();
        $this->running = false;
    }

    public function __get($name) {
        if ($name === 'execution') {
            return $this->end - $this->begin;
        } elseif ($name === 'begin') {
            return $this->begin;
        } elseif ($name === 'end') {
            return $this->end;
        } elseif ($name === 'name') {
            return $this->name;
        } elseif ($name === 'isRunning') {
            return $this->running;
        }
    }

}
