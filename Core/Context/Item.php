<?php

namespace Engine5\Core\Context;

/**
 * Description of Item
 *
 * @author barcis
 */
class Item {

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    public function __construct($_name, $_type) {
        $this->name = $_name;
        $this->type = $_type;
    }

}