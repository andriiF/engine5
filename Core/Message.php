<?php

namespace Engine5\Core;

/**
 * Description of Message
 *
 * @author barcis
 */
class Message {

    private $_messages = array();

    /**
     * @var Message
     */
    private static $_instance;

    /**
     * @var Engine5\Interfaces\Session
     */
    private $_session;

    /**
     * @throws \Exception
     */
    public static function create(\Engine5\Interfaces\Session &$session) {
        if (!is_null(self::$_instance)) {
            throw new \Exception('recreate message system');
        }
        self::$_instance = new Message($session);
    }

    public function __construct(\Engine5\Interfaces\Session &$session) {
        $this->_session = $session;
        $this->_messages = $this->_session->get('E_ms_messages');
    }

    /**
     * @param string $message
     * @param string|array $recipient
     */
    public static function addMessage($message, $recipients = array('__public__')) {
        if (!is_array($recipients)) {
            $recipients = array($recipients);
        }

        $sender = Context::currentContext()->name;

        foreach ($recipients as $recipient) {
            self::$_instance->_messages[$recipient][] = new Message\Entry($message, $sender);
        }
    }

    public static function getMessages() {
        $recipient = E_context::currentContext()->name;

        $a = isset(self::$_instance->_messages[$recipient]) && is_array(self::$_instance->_messages[$recipient]) ? self::$_instance->_messages[$recipient] : array();
        $b = isset(self::$_instance->_messages['__public__']) && is_array(self::$_instance->_messages['__public__']) ? self::$_instance->_messages['__public__'] : array();

        return array_merge($a, $b);
    }

    public static function popMessages() {
        $recipient = Context::currentContext()->name;

        $a = isset(self::$_instance->_messages[$recipient]) && is_array(self::$_instance->_messages[$recipient]) ? self::$_instance->_messages[$recipient] : array();
        $b = isset(self::$_instance->_messages['__public__']) && is_array(self::$_instance->_messages['__public__']) ? self::$_instance->_messages['__public__'] : array();

        unset(self::$_instance->_messages[$recipient], self::$_instance->_messages['__public__']);

        return array_merge($a, $b);
    }

    /**
     * @param integer $id
     * @return Message\Entry
     */
    public static function getMessage($id = 0) {
        $ms = self::getMessages();

        return $ms[$id];
    }

    /**
     * @return E_me
     */
    public static function popMessage() {
        $recipient = E_context::currentContext()->name;

        if (isset(self::$_instance->_messages[$recipient]) && is_array(self::$_instance->_messages[$recipient]) && count(self::$_instance->_messages[$recipient])) {
            return array_pop(self::$_instance->_messages[$recipient]);
        } elseif (isset(self::$_instance->_messages['__public__']) && is_array(self::$_instance->_messages['__public__']) && count(self::$_instance->_messages['__public__'])) {
            return array_pop(self::$_instance->_messages['__public__']);
        }
    }

    public static function getMessagesCount() {
        $recipient = Context::currentContext()->name;

        $a = isset(self::$_instance->_messages[$recipient]) && is_array(self::$_instance->_messages[$recipient]) ? self::$_instance->_messages[$recipient] : array();
        $b = isset(self::$_instance->_messages['__public__']) && is_array(self::$_instance->_messages['__public__']) ? self::$_instance->_messages['__public__'] : array();

        return count($a) + count($b);
    }

    public static function destroy() {
        self::$_instance->_session->set('E_ms_messages', self::$_instance->_messages);
        self::$_instance = null;
    }

}
