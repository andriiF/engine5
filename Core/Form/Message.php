<?php

namespace Engine5\Core\Form;

/**
 * Description of Message
 *
 * @author barcis
 */
class Message extends \Engine5\Pattern\Getter {

    public function __construct($text) {
        parent::__construct(array('text' => $text));
    }

}
