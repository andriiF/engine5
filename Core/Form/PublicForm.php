<?php

namespace Engine5\Core\Form;

/**
 * Description of PublicForm
 *
 * @author barcis
 */
abstract class PublicForm {

    /**
     *
     * @var Engine5\Core\Form
     */
    protected $form;

    /**
     *
     * @var Engine5\Core\Region
     */
    protected $region;
    protected $model2field = array();
    protected $field2model = array();

    public function addTab($id, $name) {
        $this->form->addTab($id, $name);
    }

    public function getTabs() {
        return $this->form->getTabs();
    }

    /**
     *
     * @var E_forms_multielement[]
     */
    protected $multielements = array();

    public function addMultiElement($group, $type, $name, array $modelField = array(), $id = 'id') {
        if (!isset($this->multielements[$group])) {
            $this->multielements[$group] = new E_forms_multielement(isset($modelField[0]) ? $modelField[0] : null, $id);
            $this->form->addElement('hidden', $group . 's', '', null)->additional('group', $group);
        }

        $this->multielements[$group]->addField($type, $name);

        unset($modelField[0]);
        if ($modelField) {
            $this->multielements[$group]->connectFieldWithModel($name, $modelField);
        }

        return $this->form->addElement($type, $name)->additional('group', $group . '_schema');
    }

    public function initMultielements() {
        foreach ($this->multielements as $group => $multielement) {
            $groups = $group . 's';

            if (strlen($this->{$groups}) > 0) {
                foreach (explode(',', $this->{$groups}) as $i) {
                    foreach ($multielement->getFields() as $field) {
                        $this->addClone($field['name'], str_replace('%#%', $i, $field['name']))->additional('group', $group);
                    }
                }
            }
        }
    }

    public function readMultielementsFromModel($model) {
        foreach ($this->multielements as $group => $multielement) {
            /* @var $multielement E_forms_multielement */
            $groups = $group . 's';

            $list = '';

            foreach ($model->{$multielement->getModelField()} as $mo) {
                $i = $mo->{ $multielement->getModelKey() };
                $list .= $i . ',';

                foreach ($multielement->getFields() as $field) {
                    $value = $mo;
                    foreach ($field['model'] as $v) {
                        @$value = &$value->{$v};
                    }


                    $this->addClone($field['name'], str_replace('%#%', $i, $field['name']), $value)
                            ->additional('group', $group)
                            ->additional('data-id', $i);
                }
            }

            if ($this->{$groups} === null) {
                $this->{$groups} = trim($list, ',');
            }
        }
    }

    public function writeMultielementsToModel(&$model) {
        /* @var $model E_model */
        foreach ($this->multielements as $group => $multielement) {
            /* @var $multielement E_forms_multielement */
            $groups = $group . 's';

            $ids = array();

            foreach ($model->{$multielement->getModelField()} as $elem) {
                $ids[$elem->{$multielement->getModelKey()}] = 1;
            }

            foreach (explode(',', $this->{$groups}) as $i) {
                $value = $model->{$multielement->getModelField()}->one($multielement->getModelKey(), $i);

                if ($value === null && preg_match('/^N[0-9]+/', $i) && isset($model::$foreign[FK::OneToMany][$multielement->getModelField()]['key'])) {
                    $key = $model::$foreign[FK::OneToMany][$multielement->getModelField()]['key'];
                    $type = $model::$foreign[FK::OneToMany][$multielement->getModelField()]['type'];

                    $rfc = new ReflectionClass($type);

                    $value = $rfc->newInstance();
                    $value->{$key} = $model->id;
                    $model->{$multielement->getModelField()}->append($value);
                }

                unset($ids[$i]);

                foreach ($multielement->getFields() as $field) {
                    $name = str_replace('%#%', $i, $field['name']);

                    if (isset($field['model']) && is_array($field['model']) && count($field['model']) > 0) {
                        $e = array_pop($field['model']);
                        if (strlen($e) > 0 && strlen($name) > 0) {
                            foreach ($field['model'] as $v) {
                                @$value = &$value->{$v};
                            }

                            if ($value instanceof E_model) {
                                $value->{$e} = $this->{$name};
                            }
                        }
                    }
                }
            }

            if (is_array($ids) && count($ids) > 0) {
                foreach ($ids as $i => $null) {
                    $value = $model->{$multielement->getModelField()}->one($multielement->getModelKey(), $i);
                    $value->delete();

                    $model->{$multielement->getModelField()}->delete($multielement->getModelKey(), $i);

                    foreach ($multielement->getFields() as $field) {
                        $name = str_replace('%#%', $i, $field['name']);
                        $this->removeElement($name);
                    }
                }
            }
        }
    }

    final public function getValueMultiFieldGroup($group) {
        $values = array();
        foreach ($this->form->getFields()->hasAdditional('group', $group) as $value) {
            /* @var $value Field */
            if (!($value instanceof E_field_hidden && $value->getName() == $group . 's')) {

                $values[$value->getName()] = ($value instanceof E_field_file) ? $value->getValue('tmp_name') : $value->getValue();
            }
        }


        return $values;
    }

    final public function __construct(\Engine5\Core\Region $region, $subname = '', $method = \Engine5\Core\Form::POST) {
        $this->region = $region;
        $name = array_reverse(explode('\\', get_class($this)))[0];
        $this->form = Manager::getInstance($region->getName())->form($name . $subname, $method);
        $this->init();
        $this->initMultielements();
    }

    final public function __get($name) {
        if (preg_match('/^F_[a-zA-Z]+.*/', $name)) {
            $fn = preg_replace('/^F_/', '', $name);
            return $this->form->getField($fn);
        }

        return $this->form->{$name};
    }

    final public function __set($name, $value) {
        $this->form->{$name} = $value;
    }

    final public function getField($name) {
        return $this->form->getField($name);
    }

    final public function getFields() {
        return $this->form->getFields();
    }

    public function readFromModel(E_model $model) {
        $this->form->readFromModel($model, $this->model2field);
        $this->readMultielementsFromModel($model);
    }

    public function writeToModel(E_model &$model) {
        $this->form->writeToModel($model, $this->field2model);
        $this->writeMultielementsToModel($model);
        Manager::resetAllRequestValues();
    }

    final public function addInfo($msg) {
        return $this->form->addInfo($msg);
    }

    final public function addError($msg) {
        return $this->form->addError($msg);
    }

    final public function asArray() {
        return $this->form->asArray();
    }

    protected final function addClone($from, $to, $defValue = null) {
        return $this->form->addClone($from, $to, $defValue);
    }

    protected final function removeField($name) {
        $this->form->removeField($name);
    }

    /**
     * @param Field $field
     * @return Field
     */
    protected final function addField(Field $field) {
        return $this->form->addField($field);
    }

    public function isValid() {
        return $this->form->isValid();
    }

    public abstract function init();
}
