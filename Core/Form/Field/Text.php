<?php

namespace Engine5\Core\Form\Field;

/**
 * Description of Text
 *
 * @author barcis
 */
class Text extends \Engine5\Core\Form\Field {

    public function __construct($name, $value = null) {
        parent::__construct($name, $value);
        parent::setShort(true);
        parent::setTag('input');
        parent::setDefaultAttribs(array('type' => 'text'));
    }

}
