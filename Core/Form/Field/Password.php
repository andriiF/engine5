<?php

namespace Engine5\Core\Form\Field;

/**
 * Description of Password
 *
 * @author barcis
 */
class Password extends Text {

    public function __construct($name, $value = null) {
        parent::__construct($name, $value);
        parent::setDefaultAttribs(array('type' => 'password'));
    }

}
