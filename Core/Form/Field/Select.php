<?php

namespace Engine5\Core\Form\Field;

/**
 * Description of Select
 *
 * @author barcis
 */
class Select extends \Engine5\Core\Form\Field {

    /**
     *
     * @var array
     */
    private $values;
    private $multi;

    public function __construct($name, $value = null) {
        parent::setShort(false);
        parent::setTag('select');
        parent::__construct($name, $value);
    }

    public function setValues(array $values) {
        $this->values = $values;
        return $this;
    }

    public function setMulti($multi) {
        if ($multi) {
            $this->addAttr('multiple', 'multiple');
        } else {
            $this->removeAttr('multiple', 'multiple');
        }
        $this->multi = $multi;
        return $this;
    }

    public function renderValue() {
        $out = '';
        if (is_array($this->values) && !empty($this->values)) {
            foreach ($this->values as $id => $value) {
                if ($this->multi && is_array($this->getValue())) {
                    $isSelected = in_array($id, $this->getValue());
                } else {
                    $isSelected = ( $this->getValue() == $id );
                }

                $isSelected = ( $isSelected ) ? ' selected=\'selected\'' : '';

                $out .= '<option value=\'' . $id . '\'' . $isSelected . '>' . $value . '</option>';
            }
        }
        return $out;
    }

}
