<?php

namespace Engine5\Core\Form\Field;

/**
 * Description of Password
 *
 * @author barcis
 */
class Checkbox extends Text {

    public function __construct($name, $value = null) {
        parent::__construct($name, $value);
        parent::setDefaultAttribs(array('type' => 'checkbox'));
    }

    public function renderValue() {
        if ($this->getValue() === 'on') {
            return ['checked' => 'checked'];
        } else {
            return [];
        }
    }

}
