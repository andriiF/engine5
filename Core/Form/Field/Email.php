<?php

namespace Engine5\Core\Form\Field;

/**
 * Description of Email
 *
 * @author barcis
 */
class Email extends Text {

    public function __construct($name, $value = null) {
        parent::__construct($name, $value);
        parent::setDefaultAttribs(array('type' => 'email'));
    }

}
