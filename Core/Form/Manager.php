<?php

namespace Engine5\Core\Form;

use \Engine5\Core\Form as Form;

/**
 * Description of Manager
 *
 * @author barcis
 */
final class Manager {

    /**
     * @var string
     */
    private $region;

    /**
     * @var Forms
     */
    private $_forms;

    /**
     * @var \Engine5\Core\Request
     */
    private $_request;

    /**
     * @var Manager
     */
    private static $instance;

    /**
     * @var array()
     */
    private $_data;

    /**
     * @var Session
     */
    private $_session;

    /**
     * @throws \Exception
     */
    public static function create($mode, \Engine5\Interfaces\Session &$session) {
        if (!is_null(self::$instance)) {
            throw new \Exception('recreate ' . __CLASS__);
        }
        self::$instance = new Manager($mode, $session);
    }

    /**
     *
     * @param string $regionName
     * @return Manager
     * @throws \Exception
     */
    public static function getInstance($regionName) {
        if (is_null(self::$instance)) {
            throw new \Exception('nie utworzono jeszcze ' . __CLASS__);
        }
        self::$instance->setRegion($regionName);
        return self::$instance;
    }

    private function __construct($mode, \Engine5\Interfaces\Session &$session) {
        $this->_session = $session;
        $this->_data = $session->get('E_formManager_data');
        $this->_request = \Engine5\Core\Request::getInstance($mode);
        $this->_forms = new Forms();
    }

    private function setRegion($regionName) {
        $this->region = $regionName;
    }

    /**
     * @param string $name
     * @return \Engine5\Core\Form
     */
    public function findForm($name) {
        if (isset($this->_forms[$this->region . '_' . $name])) {
            return $this->_forms[$this->region . '_' . $name];
        }
    }

    /**
     * @param string $name
     * @return \Engine5\Core\Form
     */
    public function form($name, $method) {
        if (!isset($this->_forms[$this->region . '_' . $name])) {
            $this->_forms[$this->region . '_' . $name] = new Form($name, $method, $this->region);
        }
        return $this->_forms[$this->region . '_' . $name];
    }

    public static function resetAllRequestValues() {
        self::$instance->_data = array();
    }

    public function getRequestValue($type, $method, $name, $form) {
        if ($method != Form::GET && $method != Form::POST) {
            throw new \Exception('metoda dla request musi być podana jako Form::POST lub Form::GET');
        }

        $tmpData = $this->_request->getValue($method, $type, $form, $this->region, $name);

        if ($tmpData === null) {
            if (isset($this->_data[$method][$type][$this->region][$form][$name])) {
                $tmpData = $this->_data[$method][$type][$this->region][$form][$name];
            }
        } else {
            $this->_data[$method][$type][$this->region][$form][$name] = $tmpData;
        }

        return $tmpData;
    }

    public function getRequestFileInfo($type, $method, $name, $form) {
        return $this->_request->getFileInfo($type, $form, $this->region, $name);
    }

    public static function destroy() {
        self::$instance->_session->set('E_formManager_data', self::$instance->_data);
        self::$instance = null;
    }

}
