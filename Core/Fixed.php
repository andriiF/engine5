<?php

namespace Engine5\Core;

/**
 * Description of Fixed
 *
 * @author barciś
 */
class Fixed {

    private $_data;

    /**
     * @var Fixed
     */
    private static $instance;

    /**
     * @var \Engine5\Interfaces\Session
     */
    private $_session;
    private $region;

    private function __construct(\Engine5\Interfaces\Session &$session) {
        $this->_session = $session;
        $this->_data = $this->_session->get('E_fixed_data');
    }

    public static function create(\Engine5\Interfaces\Session &$session) {
        if (!is_null(self::$instance)) {
            throw new \Exception('recreate ' . __CLASS__);
        }
        self::$instance = new Fixed($session);
    }

    public static function destroy() {
        self::$instance->_session->set('E_fixed_data', self::$instance->_data);
        self::$instance = null;
    }

    /**
     * @param string $regionName
     * @return Fixed
     * @throws \Exception
     */
    public static function getInstance($regionName) {
        if (is_null(self::$instance)) {
            throw new \Exception('nie utworzono jeszcze ' . __CLASS__);
        }
        self::$instance->region = $regionName;
        return self::$instance;
    }

    public function __get($name) {
        return $this->get($name);
    }

    public function __set($name, $value) {
        $this->set($name, $value, false);
    }

    public function get($name) {
        $context = $this->region;
        if (isset($this->_data[$context][$name])) {
            return $this->_data[$context][$name];
        } elseif (isset($this->_data['!public!'][$name])) {
            return $this->_data['!public!'][$name];
        }
        return null;
    }

    public function set($name, $value, $public = false) {
        $context = $public ? '!public!' : $this->region;
        $this->_data[$context][$name] = $value;
    }

}
