<?php

namespace Engine5\Core\Templater;

use Engine5\Core\Engine;

//use Engine5\Core\Auth;

/**
 * Description of smarty
 *
 * @author barcis
 */
class Smarty extends \Engine5\Core\Templater {

    /**
     * @var \Smarty
     */
    private $smarty;
    private $templateDirs = array();

    public function init(Engine\App\Config\Templater $config) {
        $this->smarty = new \Smarty();

        $this->smarty->setCompileDir(ST_ROOTDIR . '/var/compiled/');

        $this->smarty->force_compile = $config->force_compile;
        $this->smarty->error_reporting = $config->error_reporting;
        $this->smarty->debugging = $config->debugging;
        $this->smarty->autoload_filters = $config->autoload_filters;
        $this->smarty->auto_literal = false;
        $this->smarty->left_delimiter = '[{';
        $this->smarty->right_delimiter = '}]';

        if (is_array($config->plugins)) {
            foreach ($config->plugins as $plugin) {
                $func = [$plugin, 'register'];
                call_user_func($func, $this->smarty);
            }
        }

        $_app = new \stdClass();
        $_app->base_address = Engine::getCurrentAppProto() . '://' . Engine::getCurrentAppDomain() . Engine::getCurrentAppPort() . Engine::getCurrentAppBaseUrl();
        $this->smarty->assign('_app', $_app);
//        $this->smarty->assign('_user', Auth::getLoggedUser());
    }

    public function setContainer(\Engine5\Interfaces\Controller $container) {
        $this->smarty->assignByRef('container', $container);
    }

    public function setDryRun($bool) {
        $this->smarty->assign('runMode', $bool === true ? self::RUN_MODE_DRY : self::RUN_MODE_WET);
    }

    public function fetch($file, $dir, $caller = null) {
        array_unshift($this->templateDirs, $dir);
        try {
            if (isset($caller)) {
                $this->smarty->assign('this', $caller);
            }
            $this->smarty->setTemplateDir($dir);
            if (file_exists($dir . $file . '.htpl')) {
                $return = file_get_contents($dir . $file . '.htpl');
            } elseif (file_exists($dir . $file . '.html')) {
                $return = file_get_contents($dir . $file . '.html');
            } elseif (file_exists($dir . $file . '.tpl')) {
                $return = $this->smarty->fetch($file . '.tpl');
            } else {
                throw new \Exception("View file ( {$dir}{$file}.tpl) is not exists!<br/>");
            }
        } catch (\Exception $e) {
            $return = $e->getMessage();
            $return .= ' (' . $e->getFile() . ':' . $e->getLine() . ') ';
        }
        array_shift($this->templateDirs);
        if (count($this->templateDirs) > 0) {
            $dir = reset($this->templateDirs);
            $this->smarty->setTemplateDir($dir);
        }

        return $return;
    }

}
