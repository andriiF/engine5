<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Modifiers;

class Postal implements \Engine5\Interfaces\Templater\Smarty\Plugin {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('modifier', 'postal', array(__CLASS__, 'postal'));
    }

    public static function postal($string) {
        if (preg_match('/^(\d{2})(\d{3})$/', $string, $matches)) {
            unset($matches[0]);
            return implode('-', $matches);
        }

        return $string;
    }

}
