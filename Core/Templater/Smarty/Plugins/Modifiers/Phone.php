<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Modifiers;

class Phone implements \Engine5\Interfaces\Templater\Smarty\Plugin {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('modifier', 'phone', array(__CLASS__, 'phone'));
    }

    public static function phone($string) {
        if (preg_match('/^(\d{3})(\d{3})(\d{3})$/', $string, $matches)) {
            unset($matches[0]);
            return implode('-', $matches);
        }

        return $string;
    }

}
