<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Modifiers;

class Currency implements \Engine5\Interfaces\Templater\Smarty\Plugin {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('modifier', 'currency', array(__CLASS__, 'currency'));
    }

    public static function currency($number, $locale = 'pl_PL', $precision = 2) {
        $_locale = setlocale(LC_MONETARY, "0");

        $l = str_replace('-', '_', $locale);

        $_l = setlocale(LC_MONETARY, $locale, $locale . ".UTF-8", $l, $l . ".UTF-8");

        $return = money_format("%.{$precision}n", $number);
        setlocale(LC_MONETARY, $_locale);
        return $return;
    }

}
