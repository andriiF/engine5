<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Modifiers;

class Email implements \Engine5\Interfaces\Templater\Smarty\Plugin {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('modifier', 'email', array(__CLASS__, 'email'));
    }

    public static function email($string, $class = null) {
        return '<a class="mailto' . (!empty($class) ? ' ' . $class : '') . '" href="mailto:' . $string . '">' . $string . '</a>';
    }

}
