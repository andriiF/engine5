<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Debug implements \Engine5\Interfaces\Templater\Smarty\Plugin {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'p', array(__CLASS__, 'p'));
        $smarty->registerPlugin('function', 'pd', array(__CLASS__, 'pd'));
    }

    public static function p($params, \Smarty_Internal_Template &$template) {
        return \Engine5\Core\Debug::p($params);
    }

    public static function pd($params, \Smarty_Internal_Template &$template) {
        return \Engine5\Core\Debug::pd($params);
    }

}
