<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

class Url {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'url', array(__CLASS__, 'render'));
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        if (!isset($params['controller']) && !isset($params['action'])) {
            return \Engine5\Factory\Router::newInstance()->translateToUrl(\Engine5\Core\Engine::getCurrentApp()->getControllerViewArgs());
        }
        if (!isset($params['controller'])) {
            throw new \SmartyException("url wymaga parametru 'controller'!");
        }

        if (!isset($params['action'])) {
            throw new \SmartyException("url wymaga parametru 'action'!");
        }

        if (!isset($params['params'])) {
            $params['params'] = array();
        }

        $query = '';
        if (isset($params['_query'])) {
            $query = $params['_query'];
            unset($params['_query']);
        }

        $paramsNames = array_keys($params);
        $skip = array('controller', 'action', 'params', 'app', 'assign');
        if (!empty($paramsNames)) {
            foreach ($paramsNames as $paramName) {
                if (!in_array($paramName, $skip)) {
                    if (!empty($params[$paramName])) {
                        $params['params'][$paramName] = $params[$paramName];
                    }
                }
            }
        }



        if (!isset($params['app'])) {
            $params['app'] = null;
        }
        $out = \Engine5\Factory\Router::newInstance()->translateToUrl(new \Engine5\Core\ControllerViewArgs('', $params['controller'] . '::' . $params['action'], $params['params'], '', $params['app'])) . $query;

        if (!isset($params['assign'])) {
            return $out;
        } else {
            $template->assign($params['assign'], $out);
        }
    }

}
