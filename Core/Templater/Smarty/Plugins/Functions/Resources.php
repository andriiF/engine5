<?php

namespace Engine5\Core\Templater\Smarty\Plugins\Functions;

use Engine5\Core\Resource\Manager as ResourceManager;
use Engine5\Core\Engine;

class Resources {

    public static function register(\Smarty $smarty) {
        $smarty->registerPlugin('function', 'resources', array(__CLASS__, 'render'));
        $smarty->registerPlugin('function', 'resourcesadd', array(__CLASS__, 'add'));
    }

    public static function add($params, \Smarty_Internal_Template &$template) {
        if (!isset($params['type'])) {
            throw new \SmartyException('Brak typu zasobów do renderowania');
        }

        if (!isset($params['path'])) {
            throw new \SmartyException('Brak scieżki zasobów do renderowania');
        }

        if (!in_array($params['type'], ResourceManager::getResourceTypes())) {
            throw new \Exception('typ zasobu musi być js lub css');
        }
        if (!isset($params['priority'])) {
            $params['priority'] = 10;
        }

        ResourceManager::getInstance('template')->add($params['type'], $params['path'], $params['priority']);
    }

    public static function render($params, \Smarty_Internal_Template &$template) {
        if (!isset($params['type'])) {
            throw new \SmartyException('Brak typu zasobów do renderowania');
        }

        if (!in_array($params['type'], ResourceManager::getResourceTypes())) {
            throw new \Exception('typ zasobu musi być js, css lub app');
        }

        $skin = Engine::getCurrentApp()->getCurrentSkin();
        $app = Engine::getCurrentAppName();

        $code = ResourceManager::renderAll($params['type'], $app, $skin);


        return $code;
    }

}
