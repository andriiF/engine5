<?php

namespace Engine5\Core\Templater\Angular\Directive;

class NgRepeat extends \Engine5\Core\Templater\Angular\Directive {

    public function apply() {
        $attrs = $this->attributes();
        $attrValue = $attrs[$this->name];
        if (!preg_match('|(?<item>[a-z]*)\s*in\s*(?<array>[a-z\.]*)\s*(\|(?<filters>\s*.*))?|im', $attrValue, $matches)) {
            throw new \Exception(sprintf('Invalid value "%s" for ' . $this->name . ' directive', $attrValue));
            return;
        }

        $item = trim($matches['item']);
        $array = trim($matches['array']);
        $filters = isset($matches['filters']) ? explode('|', $matches['filters']) : [];

        $pathEntries = explode('.', $array);
        $s = $this->scope;

        foreach ($pathEntries as $path) {
            $s = $s[$path];
            if ($s === null) {
                break;
            }
        }

        if ($s === null) {
            return;
        }

        $parent = $this->element->parentNode;

        $nodes = [];

        foreach ($filters as $filter) {
            $params = explode(':', $filter);
            $name = array_shift($params);

            $func = $this->module->filter($name);
            $s = call_user_func_array($func, [$s, $params]);
        }

        foreach ($s as $index => $value) {
            $node = $this->element->cloneNode(true);
            $node->removeAttribute($this->name);

            $parent->insertBefore($node, $this->element);
            $scope = clone $this->scope;
            $scope[$item] = $value;
            $scope[$item] = $value;
            $scope['$index'] = $index;

            $nodes [] = $this->module->parser->parse($node, $scope);
        }
        $parent->removeChild($this->element);
        $this->node->nodes($nodes);
    }

}
