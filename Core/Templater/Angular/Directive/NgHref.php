<?php

namespace Engine5\Core\Templater\Angular\Directive;

class NgHref extends \Engine5\Core\Templater\Angular\Directive {

    public function __construct($element, $scope, $module, $attrName) {
        parent::__construct($element, $scope, $module, $attrName);
        $href = $this->element->attributes->getNamedItem($this->name);
        if (!($href instanceof \DOMAttr)) {
            return;
        }
        $attr = $this->element->ownerDocument->createAttribute('href');
        $attr->value = $href->value;
        $this->element->appendChild($attr);
        $this->element->removeAttribute($this->name);
    }

    public function apply() {

    }

}
