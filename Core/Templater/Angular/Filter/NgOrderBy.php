<?php

namespace Engine5\Core\Templater\Angular\Filter;

class NgOrderBy extends \Engine5\Core\Templater\Angular\Filter {

    public static function apply($data, array $params = []) {
        $column = trim($params[0], '\'');
        usort($data, function($a, $b) use($column) {
            if ($a->{$column} === $b->{$column}) {
                return 0;
            } else {
                return $a->{$column} > $b->{$column} ? 1 : -1;
            }
        });

        return $data;
    }

}
