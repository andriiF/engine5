<?php

namespace Engine5\Core\Templater\Angular;

class Parse {

    protected $module = null;

    public function __construct($module) {
        $this->module = $module;
    }

    /**
     *
     * @param \DOMNode $node
     * @param Scope $scope
     * @return \Engine5\Core\Templater\Angular\DOMNode
     */
    public function parse(\DOMNode $node, $scope) {
        $directive = $this->detectDirective($node, $scope);

        $nodes = [];
        // traverse nodes
        if ($node->childNodes) {
            for ($i = 0; $i < $node->childNodes->length; $i++) {
                $childNode = $node->childNodes->item($i);
                $nodes [] = $this->parse($childNode, $scope);
            }
        }
        if ($node->attributes) {
            for ($i = 0; $i < $node->attributes->length; $i++) {
                $childNode = $node->attributes->item($i);
                $nodes [] = $this->parse($childNode, $scope);
            }
        }
        $directive->nodes($nodes);
        return $directive;
    }

    protected function detectDirective(\DOMNode $node, $scope) {
        $directives = $this->module->directives();
        $nodeDirectives = [];

        if ($node instanceof \DOMElement) {
            // if element - check attributes
            for ($j = 0; $j < $node->attributes->length; $j++) {
                $attribute = $node->attributes->item($j);
                $attrName = (strpos($attribute->name, 'data-') === 0) ? str_replace('data-', '', $attribute->name) : $attribute->name;

                foreach ($directives as $name => $directive) {
                    if (strpos($directive['restrict'], 'A') !== false) {
                        if ($name == $attrName) {
//                            if (strpos($attribute->name, 'data-') === 0) {
//                                $attr = $node->ownerDocument->createAttribute($attrName);
//                                $attr->value = $attribute->value;
//                                $node->removeAttribute($attribute->name);
//                                $node->appendChild($attr);
//                            }
                            $class = $directive['class'];
                            $nodeDirectives [] = new $class($node, $scope, $this->module, $attribute->name);
                        }
                    }
                }
            }
        } elseif ($node instanceof \DOMText) {
            // text
            $nodeDirectives [] = new Directive\NgBind($node, $scope, $this, '');
        }

        return new DOMNode($node, $nodeDirectives);
    }

}
