<?php

namespace Engine5\Core\Templater\Angular;

class Module {

    protected $document = null;
    protected $name = null;
    protected $options = null;

    /**
     *
     * @var Parse
     */
    public $parser = null;
    protected $rootScope = null;
    protected $directives = [];
    protected $filters = [];

    /**
     *
     * @var \Engine5\Core\Templater\Angular
     */
    protected $templater;

    public function __construct($name, $options = [], $templater) {
        $this->name = $name;
        $this->options = $options;
        $this->templater = $templater;
    }

    public function templater() {
        return $this->templater;
    }

    /**
     *
     * @return Directive
     */
    public function directives() {
        return $this->directives;
    }

    public function options($options = null) {
        if ($options !== null) {
            $this->options = $options;
        }

        return $this->options;
    }

    public function directive($name, $func) {
        $this->directives[$name] = $func;
    }

    public function filter($name, $func = null) {
        if ($func === null && isset($this->filters[$name])) {
            return [$this->filters[$name], 'apply'];
        }
        $this->filters[$name] = $func;
    }

    public function scope(array $scope = null) {
        if ($scope !== null) {
            $this->rootScope = new Scope($scope);
        }

        if ($this->rootScope === null) {
            $this->rootScope = new Scope([]);
        }
        return $this->rootScope;
    }

    public function html() {
        $filename = $this->options['dir'] . DIRECTORY_SEPARATOR . $this->options['file'];

        $this->document = new \DOMDocument();
        $this->document->loadHTMLFile($filename);

        $this->parser = new Parse($this);

        $node = $this->parser->parse($this->document, $this->scope());
        $node->apply();

        return $this->document->saveHtml();
    }

}
