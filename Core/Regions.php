<?php

namespace Engine5\Core;

/**
 * Description of Regions
 *
 * @author barcis
 */
class Regions extends \Engine5\Pattern\Lists {

    /**
     * @return string
     */
    public function render($runMode) {
        $output = "";
        foreach ($this->items as $region) {
            /* @var $region Region */
            $output .= $region->render($runMode);
        }
        return $output;
    }

    public function regions() {
        return array_keys($this->items);
    }

    /**
     * @return string
     */
    public function renderRegion($runMode, $id) {
        return $this->items[$id]->render($runMode);
    }

}
