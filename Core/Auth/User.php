<?php

namespace Engine5\Core\Auth;

/**
 * Description of Users
 *
 * @author barcis
 */
class User {

    private $username;

    public function __construct($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

}
