<?php

namespace Engine5\Core;

/**
 * Description of Request
 *
 * @author barcis
 */
class Request {

    /**
     * @var Request
     */
    private static $instance = null;
    private $mode = null;

    /**
     * @var array
     */
    private $requests = array();

    /**
     * @var array
     */
    private $files = array();

    /**
     * @var array
     */
    private $reqAjax = array();

    /**
     * @var array
     */
    private static $activeRegions = array();

    private function __construct($mode) {
        $this->mode = $mode;
        $this->files = $_FILES;
        $this->requests[Form::POST] = $_POST;
        $this->requests[Form::GET] = $_GET;

        if ($mode == Engine\Entrypoint::REST) {
            $method = Engine::getRequestMethod();
            if ($method != 'get') {
                $tmp = file_get_contents("php://input");
                if ($tmp) {
                    $this->requests[$method] = json_decode($tmp);
                    $this->requests['raw_' . $method] = $tmp;
                }
            } else {
                $query = explode('&', Engine::getQueryString());
                $params = array();

                if ($query) {
                    foreach ($query as $param) {
                        @list($name, $value) = explode('=', $param);

                        $k = urldecode($name);
                        if (isset($params[$k])) {
                            if (!is_array($params[$k])) {
                                $params[$k] = [ $params[$k]];
                            }
                            $params[$k][] = urldecode($value);
                        } else {
                            $params[$k] = urldecode($value);
                        }
                    }
                    $this->requests[$method] = $params;
                    $this->requests['raw_' . $method] = $query;
                }
            }
            if ($_FILES) {
                $this->requests['files'] = $_FILES;
            }
            $this->reqAjax = $this->requests;
        } elseif ($mode == Engine\Entrypoint::AJAX ||
                $mode == Engine\Entrypoint::VIEW) {
            $this->reqAjax = array(
                Form::POST => array(),
                E_form::GET => array(),
                'files' => $_FILES);
        }

//        $_POST = 'empty';
//        $_GET = 'empty';
//        $_FILES = 'empty';

        if ($mode != Engine\Entrypoint::REST) {

            foreach ($this->requests as $requestType => $request) {
                foreach ($request as $type => $regions) {
                    if (!class_exists($type)) {
                        if ($mode == Engine\Entrypoint::AJAX || $mode == Engine\Entrypoint::VIEW) {
                            if ($requestType == Form::GET) {
                                $this->reqAjax[$requestType][$type] = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($this->requests[$requestType][$type])), null, 'UTF-8');
                            } else {
                                $this->reqAjax[$requestType][$type] = $this->requests[$requestType][$type];
                            }
                            unset($this->requests[$requestType][$type]);
                        } else {
                            $this->requests[$requestType][$type] = htmlspecialchars($this->requests[$requestType][$type]);
                        }

                        continue;
                    }

                    $rfc = new \ReflectionClass($type);
                    if ($rfc->implementsInterface('Engine5\Interfaces\Form\Field\Action')) {
                        foreach ($regions as $region => $forms) {
                            foreach ($forms as $form => $fields) {
                                foreach (array_keys($fields) as $field) {
                                    if (!isset(self::$activeRegions[$region])) {
                                        self::$activeRegions[$region] = [];
                                    }

                                    self::$activeRegions[$region][] = ['form' => $form, 'field' => $field, 'type' => $type];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getParams() {
        return self::$instance->requests;
    }

    public static function getAjaxParams() {
        if (self::$instance->mode == Engine\Entrypoint::AJAX ||
                self::$instance->mode == Engine\Entrypoint::VIEW ||
                self::$instance->mode == Engine\Entrypoint::REST) {
            return self::$instance->reqAjax;
        } else {
            return array(Form::POST => array(), Form::GET => array());
        }
    }

    /**
     *
     * @param string $mode
     * @return Request
     * @throws Exception
     */
    public static function getInstance($mode) {
        if (!is_null(self::$instance)) {
            throw new Exception('redeclare request');
        }
        self::$instance = new Request($mode);
        return self::$instance;
    }

    public function getValue($method, $type, $form, $region, $name) {
        if ($method != Form::GET && $method != Form::POST) {
            throw new \Exception('metoda dla request musi być podana jako E_form::POST lub E_form::GET');
        }

        if ($type == 'E_field_checkbox') {
            return isset($this->requests[$method][$type][$region][$form][$name]) ? 'on' : 'off';
        }
        return isset($this->requests[$method][$type][$region][$form][$name]) ? $this->requests[$method][$type][$region][$form][$name] : null;
    }

    public function getFileInfo($type, $form, $region, $name) {
        if (!isset($this->files[$type]['name'][$region][$form][$name])) {
            return null;
        }

        $fileInfo = array();
        $fileInfoKeys = array(
            'name',
            'type',
            'tmp_name',
            'error',
            'size'
        );
        foreach ($fileInfoKeys as $key) {
            $fileInfo[$key] = $this->files[$type][$key][$region][$form][$name];
        }

        return $fileInfo;
    }

    public static function isActiveRegions($name) {
        return isset(self::$activeRegions[$name]) ? self::$activeRegions[$name] : false;
    }

}
