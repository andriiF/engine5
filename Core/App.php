<?php

namespace Engine5\Core;

/**
 * Description of App
 *
 * @author barcis
 */
abstract class App implements \Engine5\Interfaces\App {

    /**
     * @var Controller
     */
    protected $controller;

    /**
     * @var \Engine5\Interfaces\Templater
     */
    protected $templater;

    /**
     * @var \Engine5\Core\Engine\App\Config
     */
    protected $config;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $theme;

    /**
     * @var string
     */
    protected $skin;

    /**
     * @var string
     */
    protected $cva;

    public function __construct($pathinfo, $method = null, $uri = null) {
        $this->name = Engine::getCurrentAppName();
        $this->config = new Engine\App\Config(new Engine\Config\Parser\Yaml('app.config.yml'));
        $this->skin = $this->config->defaultSkin;

        $router = \Engine5\Factory\Router::newInstance();
        $this->templater = \Engine5\Factory\Templater::newInstance();
        try {
            $this->cva = $router->translateFromUrl($pathinfo, $method, $this->name, $uri);
            $this->theme = $this->cva->theme;
        } catch (\Exception $e) {
            if ($this->config->httpErrorMap) {
                $httpErrorMap = (array) $this->config->httpErrorMap;
                if (isset($httpErrorMap[404])) {
                    $newController = $httpErrorMap[404];
                    if ($newController) {
                        $this->cva = new ControllerViewArgs('\\Engine5\\Core\\Controller', $newController);
                        $this->theme = 'default';
                        return;
                    }
                }
            }
            throw $e;
        }
    }

    /**
     *
     * @return ControllerViewArgs
     */
    public function getControllerViewArgs() {
        return $this->cva;
    }

    public function run() {

        $this->controller = $this->cva->getControllerInstance($this->templater);

        if (Engine::getEntryPoint() != Engine\Entrypoint::AJAX && Engine::getEntryPoint() != Engine\Entrypoint::REST) {
            $this->controller->__preInit($this->cva);

            if ($this->controller->hasErrors() && $this->config->httpErrorMap) {
                $httpErrorMap = (array) $this->config->httpErrorMap;
                $errors = $this->controller->getErrors();
                $newController = null;
                foreach ($errors as $error) {
                    if (isset($httpErrorMap[$error['error']])) {
                        $newController = $httpErrorMap[$error['error']];
                        break;
                    }
                }
                if ($newController) {
                    $this->cva = new ControllerViewArgs('\\Engine5\\Core\\Controller', $newController);
                    $this->theme = 'default';
                    $this->controller = $this->cva->getControllerInstance($this->templater);
                    $this->controller->__preInit($this->cva);
                }
            }
        }

        $this->controller->__init();

        if (method_exists($this->controller, $this->cva->action)) {
            call_user_func(array($this->controller, $this->cva->action));
        }

        Context::unsetContext();
        return $this;
    }

    /**
     *
     * @return \Engine5\Core\Engine\App\Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     *
     * @return string
     */
    public function getCurrentSkin() {
        return $this->skin;
    }

    public function setLanguageId($id) {
        $this->config->setLanguageId($id);
    }

    public function setLanguageCurrency($code) {
        $this->config->setLanguageCurrency($code);
    }

    public function __toString() {
        try {
            return $this->render();
        } catch (\Exception $e) {
            http_response_code(404);
            echo '<pre>E5APP EXCEPTION\n' . $e->getMessage() . '</pre>';
        }
    }

    private function render() {
        $dir = ST_APPDIR . '/skins/' . $this->skin . '/view/';

        if (!file_exists($dir)) {
            throw new \Exception('Skins directory does not exist!<br/>' . $dir);
        }

        $this->templater->setContainer($this->controller);

        $this->templater->setDryRun(true);
        $this->templater->fetch($this->theme, $dir, $this);
        $this->templater->setDryRun(false);
        $html = $this->templater->fetch($this->theme, $dir, $this);

        if (file_exists(ST_VAR . '/debug') && isset($_COOKIE['e5dbg']) && Engine::getEntryPoint() === Engine\Entrypoint::WWW) {
            $expectValue = trim(file_get_contents(ST_VAR . '/debug'));
            $cookieValue = trim($_COOKIE['e5dbg']);

            if ($cookieValue == $expectValue) {
                $dir = ST_VENDOR . '/batusystems/engine5/Debug/view/';

                $_edg5 = new \stdClass();

                $_edg5->app = new \stdClass();
                $_edg5->app->name = \Engine5\Core\Engine::getCurrentAppName();
                $_edg5->app->maxlifetime = ini_get("session.gc_maxlifetime") / 60;

                $_edg5->app->config = \Engine5\Core\Engine::getConfig()->configs[$_edg5->app->name];

                if ($_edg5->app->config['database']['type'] === 'yaml') {
                    $dbcf = ST_CONFIGDIR . '/' . $_edg5->app->config['database']['file'];
                    $_edg5->app->config['database']['config'] = \Engine5\Tools\Yaml::parseFile($dbcf);
                }

                $_edg5->app->execution_time = Debug::endTimeLogSession('_all')->execution;

                $debug = $this->templater->fetch('debug', $dir, $_edg5) . PHP_EOL;
                return substr_replace($html, $debug, strrpos($html, '</body>'), 0);
            }
        }
        return $html;
    }

    final protected function setResourceCompression($value) {
        Resource\Manager::getInstance('APP' . $this->name)->setResourceCompression($value);
    }

    final protected function getResourceCompression() {
        return Resource\Manager::getInstance('APP' . $this->name)->getResourceCompression();
    }

    final private function resource($type, $name, $priority) {
        Resource\Manager::getInstance('APP' . $this->name)->add($type, $name, $priority);
    }

    final protected function js($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_JS, $name, $priority);
    }

    final protected function app($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_APP, $name, $priority);
    }

    final protected function css($name, $priority = 5) {
        return $this->resource(Resource\Manager::RESOURCE_CSS, $name, $priority);
    }

}
