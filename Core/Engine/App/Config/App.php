<?php

namespace Engine5\Core\Engine\App\Config;

/**
 * Description of App
 *
 * @author barcis
 */
class App extends Engine5\Core\Engine\App\Config {

    public function setLanguageId($id) {
        $this->set('defaultLanguage', $id);
    }

    public function setLanguageCurrency($code) {
        $this->set('defaultCurrency', $code);
    }

}
