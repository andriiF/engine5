<?php

namespace Engine5\Core\Engine\Config\Parser;

/**
 * Description of Yaml
 *
 * @author barcis
 *
 */
class Yaml extends \Engine5\Pattern\Getter implements \Engine5\Interfaces\App\Config {

    public function __construct($filename, $field = null, $subfield = null) {
        $app = ucfirst(\Engine5\Core\Engine::getCurrentAppName());
        $config = \Engine5\Tools\Yaml::parseFile(ST_CONFIGDIR . '/' . $filename);

        if (!empty($subfield)) {
            parent::__construct($config[$field][$subfield]);
        } elseif (!empty($field)) {
            parent::__construct($config[$field]);
        } else {
            parent::__construct($config);
        }
    }

}
