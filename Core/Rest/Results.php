<?php

namespace Engine5\Core\Rest;

/**
 * Description of Results
 *  @method Result Continue(mixed $data) 100: Kontynuuj – prośba o dalsze wysyłanie zapytania
 *  @method Result SwitchingProtocols(mixed $data) 101: Zmiana protokołu
 *  @method Result ConnectionTimedOut(mixed $data) 110: Przekroczono czas połączenia. Serwer zbyt długo nie odpowiada.
 *  @method Result ConnectionRefused(mixed $data) 111: Serwer odrzucił połączenie
 *
 *  @method Result Ok(mixed $data) 200: Zawartość żądanego dokumentu
 *  @method Result Created(mixed $data) 201: Utworzono – wysłany dokument został zapisany na serwerze
 *  @method Result Accepted(mixed $data) 202: Przyjęto – zapytanie zostało przyjęte do obsłużenia, lecz jego zrealizowanie jeszcze się nie skończyło
 *  @method Result NonAuthoritativeInformation(mixed $data) 203: Informacja nieautorytatywna – zwrócona informacja nie odpowiada dokładnie odpowiedzi pierwotnego serwera, lecz została utworzona z lokalnych bądź zewnętrznych kopii
 *  @method Result NoContent(mixed $data) 204: Brak zawartości – serwer zrealizował zapytanie klienta i nie potrzebuje zwracać żadnej treści
 *  @method Result ResetContent(mixed $data) 205: Przywróć zawartość – serwer zrealizował zapytanie i klient powinien przywrócić pierwotny wygląd dokumentu
 *  @method Result PartialContent(mixed $data) 206: Część zawartości – serwer zrealizował tylko część zapytania typu GET, odpowiedź musi zawierać nagłówek Range informujący o zakresie bajtowym zwróconego elementu
 *
 *  @method Result MultipleChoices(mixed $data) 300: Wiele możliwości – istnieje więcej niż jeden sposób obsłużenia danego zapytania, serwer może podać adres zasobu, który pozwala na wybór jednoznacznego zapytania spośród możliwych
 *  @method Result MovedPermanently(mixed $data) 301: Trwale przeniesiony – żądany zasób zmienił swój URI i w przyszłości zasób powinien być szukany pod wskazanym nowym adresem
 *  @method Result Found(mixed $data) 302: Znaleziono – żądany zasób jest chwilowo dostępny pod innym adresem a przyszłe odwołania do zasobu powinny być kierowane pod adres pierwotny
 *  @method Result SeeOther(mixed $data) 303: Zobacz inne – odpowiedź na żądanie znajduje się pod innym URI i tam klient powinien się skierować. To jest właściwy sposób przekierowywania w odpowiedzi na żądanie metodą POST.
 *  @method Result NotModified(mixed $data) 304: Nie zmieniono – zawartość zasobu nie podległa zmianie według warunku przekazanego przez klienta (np. data ostatniej wersji zasobu pobranej przez klienta – pamięć podręczna przeglądarki)
 *  @method Result UseProxy(mixed $data) 305: Użyj serwera proxy – do żądanego zasobu trzeba odwołać się przez serwer proxy podany w nagłówku Location odpowiedzi
 *  @method Result SwitchProxy(mixed $data) 306: Kod nieużywany, aczkolwiek zastrzeżony dla starszych wersji protokołu
 *  @method Result TemporaryRedirect(mixed $data) 307: Tymczasowe przekierowanie – żądany zasób znajduje się chwilowo pod innym adresem URI, odpowiedź powinna zawierać zmieniony adres zasobu, na który klient zobowiązany jest się przenieść
 *  @method Result TooManyRedirects(mixed $data) 310: Zbyt wiele przekierowań.
 *
 *  @method Result BadRequest(mixed $data) 400: Nieprawidłowe zapytanie – żądanie nie może być obsłużone przez serwer z powodu błędnej składni zapytania
 *  @method Result Unauthorized(mixed $data) 401: Nieautoryzowany dostęp – żądanie zasobu, który wymaga uwierzytelnienia
 *  @method Result PaymentRequired(mixed $data) 402: Wymagana opłata – odpowiedź zarezerwowana na przyszłość
 *  @method Result Forbidden(mixed $data) 403: Zabroniony – serwer zrozumiał zapytanie lecz konfiguracja bezpieczeństwa zabrania mu zwrócić żądany zasób
 *  @method Result NotFound(mixed $data) 404: Nie znaleziono – serwer nie odnalazł zasobu według podanego URL ani niczego co by wskazywało na istnienie takiego zasobu w przeszłości
 *  @method Result MethodNotAllowed(mixed $data) 405: Niedozwolona metoda – metoda zawarta w żądaniu nie jest dozwolona dla wskazanego zasobu, odpowiedź zawiera też listę dozwolonych metod
 *  @method Result NotAcceptable(mixed $data) 406: Niedozwolone – zażądany zasób nie jest w stanie zwrócić odpowiedzi mogącej być obsłużonej przez klienta według informacji podanych w zapytaniu
 *  @method Result ProxyAuthenticationRequired(mixed $data) 407: Wymagane uwierzytelnienie do serwera pośredniczącego (ang. proxy) – analogicznie do kodu 401, dotyczy dostępu do serwera proxy
 *  @method Result RequestTimeout(mixed $data) 408: Koniec czasu oczekiwania na żądanie – klient nie przesłał zapytania do serwera w określonym czasie
 *  @method Result Conflict(mixed $data) 409: Konflikt – żądanie nie może być zrealizowane, ponieważ występuje konflikt z obecnym statusem zasobu, ten kod odpowiedzi jest zwracany tylko w przypadku podejrzewania przez serwer, że klient może nie znaleźć przyczyny błędu i przesłać prawidłowego zapytania
 *  @method Result Gone(mixed $data) 410: Zniknął (usunięto) – zażądany zasób nie jest dłużej dostępny i nie znany jest jego ewentualny nowy adres URI; klient powinien już więcej nie odwoływać się do tego zasobu
 *  @method Result LengthRequired(mixed $data) 411: Wymagana długość – serwer odmawia zrealizowania zapytania ze względu na brak nagłówka Content-Length w zapytaniu; klient może powtórzyć zapytanie dodając doń poprawny nagłówek długości
 *  @method Result PreconditionFailed(mixed $data) 412: Warunek wstępny nie może być spełniony – serwer nie może spełnić przynajmniej jednego z warunków zawartych w zapytaniu
 *  @method Result RequestEntityTooLarge(mixed $data) 413: Encja zapytania zbyt długa – całkowita długość zapytania jest zbyt długa dla serwera
 *  @method Result RequestUriTooLong(mixed $data) 414: Adres URI zapytania zbyt długi – długość zażądanego URI jest większa niż maksymalna oczekiwana przez serwer
 *  @method Result UnsupportedMediaType(mixed $data) 415: Nieznany sposób żądania – serwer odmawia przyjęcia zapytania, ponieważ jego składnia jest niezrozumiała dla serwera
 *  @method Result RequestedRangeNotSatisfiable(mixed $data) 416: Zakres bajtowy podany w zapytaniu nie do obsłużenia – klient podał w zapytaniu zakres, który nie może być zastosowany do wskazanego zasobu
 *  @method Result ExpectationFailed(mixed $data) 417: Oczekiwana wartość nie do zwrócenia – oczekiwanie podane w nagłówku Expect żądania nie może być spełnione przez serwer lub – jeśli zapytanie realizuje serwer proxy – serwer ma dowód, że oczekiwanie nie będzie spełnione przez następny w łańcuchu serwer realizujący zapytanie
 *  @method Result IMaTeapot(mixed $data) 418: "Jestem czajnikiem" - tzw. easter egg. Zdefiniowany w 1998. Obecnie nie jest implementowany do serwerów HTTP, ale znane są takie przypadki
 *
 *  @method Result InternalServerError(mixed $data) 500:
 *  @method Result BadGateway(mixed $data) 501:
 *  @method Result NotImplemented(mixed $data) 502:
 *  @method Result ServiceUnavailable(mixed $data) 503:
 *  @method Result GatewayTimeout(mixed $data) 504:
 *  @method Result HttpVersionNotSupported(mixed $data) 505:
 *  @method Result VariantAlsoNegotiates(mixed $data) 506:
 *  @method Result InsufficientStorage(mixed $data) 507:
 *  @method Result LoopDetected(mixed $data) 508:
 *  @method Result NotExtended(mixed $data) 509:
 *  @method Result NetworkAuthenticationRequired(mixed $data) 510:
 *
 *  @method Result DataValidationFaild(mixed $data) 600:
 *  @method Result DataDuplicated(mixed $data) 601:
 *  @method Result DataToShort(mixed $data) 602:
 *  @method Result DataNotEqual(mixed $data) 603:
 *  @method Result InputNotComplete(mixed $data) 604:
 *  @method Result DataNotActive(mixed $data) 605:
 *
 *  @method Result DataValidationOk(mixed $data) 250:
 *
 * @author barcis
 */
class Results {

    private static $codes = [
        'Continue' => 100,
        'SwitchingProtocols' => 101,
        'ConnectionTimedOut' => 110,
        'ConnectionRefused' => 111,
        'Ok' => 200,
        'Created' => 201,
        'Accepted' => 202,
        'NonAuthoritativeInformation' => 203,
        'NoContent' => 204,
        'ResetContent' => 205,
        'PartialContent' => 206,
        'MultipleChoices' => 300,
        'MovedPermanently' => 301,
        'Found' => 302,
        'SeeOther' => 303,
        'NotModified' => 304,
        'UseProxy' => 305,
        'SwitchProxy' => 306,
        'TemporaryRedirect' => 307,
        'TooManyRedirects' => 310,
        'BadRequest' => 400,
        'Unauthorized' => 401,
        'PaymentRequired' => 402,
        'Forbidden' => 403,
        'NotFound' => 404,
        'MethodNotAllowed' => 405,
        'NotAcceptable' => 406,
        'ProxyAuthenticationRequired' => 407,
        'RequestTimeout' => 408,
        'Conflict' => 409,
        'Gone' => 410,
        'LengthRequired' => 411,
        'PreconditionFailed' => 412,
        'RequestEntityTooLarge' => 413,
        'RequestUriTooLong' => 414,
        'UnsupportedMediaType' => 415,
        'RequestedRangeNotSatisfiable' => 416,
        'ExpectationFailed' => 417,
        'IMaTeapot' => 418,
        'InternalServerError' => 500,
        'NotImplemented' => 501,
        'BadGateway' => 502,
        'ServiceUnavailable' => 503,
        'GatewayTimeout' => 504,
        'HttpVersionNotSupported' => 505,
        'VariantAlsoNegotiates' => 506,
        'InsufficientStorage' => 507,
        'LoopDetected' => 508,
        'NotExtended' => 509,
        'NetworkAuthenticationRequired' => 510,
        'DataValidationFaild' => 600,
        'DataDuplicated' => 601,
        'DataToShort' => 602,
        'DataNotEqual' => 603,
        'InputNotComplete' => 604,
        'DataNotActive' => 605,
        'DataValidationOk' => 250
    ];

    public function __call($name, $arguments) {

        if (!isset(self::$codes[$name])) {
            throw new \BadMethodCallException($name);
        }

        return new Result(isset($arguments[0]) ? $arguments[0] : null, self::$codes[$name], $name);
    }

    public static function instance() {
        return new self();
    }

}
