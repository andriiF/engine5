<?php

namespace Engine5\Core\Rest;

/**
 * Description of Api
 *
 * @author barcis
 */
class Api {

    protected $params = array();
    protected $data = array();

    public final function __construct($params = array(), $data = array()) {
        $this->params = $params;
        $this->data = $data;
        $this->init();
    }

    public function init() {

    }

}
