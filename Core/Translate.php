<?php

namespace Engine5\Core;

/**
 * Description of Fixed
 *
 * @author barciś
 */
class Translate {

    public static function _($text) {

        if (method_exists(Engine::getCurrentApp(), 'getLanguageCode')) {

            $code = Engine::getCurrentApp()->getLanguageCode();

            if (!file_exists(ST_APPDIR . '/locale/' . $code . '/')) {
                mkdir(ST_APPDIR . '/locale/' . $code . '/', 0777);
            }
            if (!file_exists(ST_APPDIR . '/locale/' . $code . '/default.po')) {
                touch(ST_APPDIR . '/locale/' . $code . '/default.po');
            }

            $parser = \Sepia\PoParser::parseFile(ST_APPDIR . '/locale/' . $code . '/default.po');
            $entries = $parser->getEntries();

            if (!isset($entries[$text])) {
                $parser->setEntry($text, ["msgid" => [$text], "msgstr" => [$text]], true);
                $parser->writeFile(ST_APPDIR . '/locale/' . $code . '/default.po');

                $entries[$text]["msgstr"][0] = $text;
            }

            return $entries[$text]["msgstr"][0];
        }

        return $text;
    }

}
