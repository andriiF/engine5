<?php

namespace Engine5\Core;

/**
 * Description of ControllerViewArgs
 *
 * @author barcis
 * @property-read $controller string
 * @property-read $action string
 * @property-read $theme string
 * @property-read $params array
 * @property-read $region array
 * @property-read $view array
 */
class ControllerViewArgs extends \Engine5\Pattern\Getter {

    private $restArgsName = array();
    private $_controller;

    public function __construct($_controller, $_action, array $_params = array(), $_theme = '', $_app = '', $cache = false) {
        list($_region, $_view) = explode('::', $_action);
        parent::__construct(
                array(
                    'app' => $_app,
                    'controller' => $_controller,
                    'action' => $_action,
                    'region' => $_region,
                    'view' => $_view,
                    'theme' => $_theme,
                    'params' => $_params,
                    'cache' => $cache)
        );
    }

    /**
     * @return string
     */
    public function getControllerView() {
        return $this->action;
    }

    public function getControllerRegion() {
        return $this->region;
    }

    /**
     * @return string
     */
    public function getAppView() {
        return $this->app;
    }

    /**
     * @return \Engine5\Interfaces\Controller
     */
    public function getController() {
        return $this->_controller;
    }

    /**
     *  @return EI_theme
     */
    public function getControllerInstance(\Engine5\Interfaces\Templater $templater) {
        if ($this->_controller === null) {

            Context::setContext(Context::CONTEXT_CONTROLLER, $this->controller);
            $rfc = new \ReflectionClass($this->controller);
            $this->_controller = $rfc->newInstance($templater, $this->params, $this->cache);
        }
        return $this->_controller;
    }

    public function setRestArgsNames(array $args) {
        $this->restArgsName = $args;
    }

    public function getRestArgsNames() {
        return $this->restArgsName;
    }

    public function setParam($key, $value) {
        $params = is_array($this->params) ? $this->params : array();
        $params[$key] = $value;
        $this->setData([
            'controller' => $this->controller,
            'action' => $this->action,
            'theme' => $this->theme,
            'params' => $params
        ]);
    }

}
