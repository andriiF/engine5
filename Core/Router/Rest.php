<?php

namespace Engine5\Core\Router;

use \Engine5\Core\Engine;
use \Engine5\Core\Request;
use \Engine5\Tools\Yaml;
use \Engine5\Core\ControllerViewArgs as ControllerViewArgs;
use \Engine5\Core\Engine\Entrypoint as Entrypoint;

/**
 * Description of Rest
 *
 * @author barcis
 */
class Rest implements \Engine5\Interfaces\Router {

    private $config = array();
    private $urls = array();
    private $controllerView = array();

    public function __construct() {
        $appname = Engine::getCurrentAppName();
        $this->config = Yaml::parseFile(ST_CONFIGDIR . '/rest.config.yml');

        if (!isset($this->config['controller'])) {
            $this->config['controller'] = '\\Engine5\\Core\\Controller\\Rest';
        }

        if (!isset($this->config['theme'])) {
            $this->config['theme'] = 'ajax';
        }

        if (isset($this->config['urls']) && is_array($this->config['urls'])) {
            $this->urls = $this->config['urls'];
        }
    }

    public function getBaseForAplication($app = null) {
        $base = '';

        $currentAppname = Engine::getCurrentAppName();
        if (empty($app) || $app == $currentAppname) {
            $urls = $this->config ['urls'];
            $base = Engine::getCurrentAppProto() .
                    '://' .
                    Engine::getCurrentAppDomain() .
                    Engine::getCurrentAppPort() .
                    Engine::getCurrentAppBaseUrl();
        } else {
            $configs = Engine::getConfig()->configs;
            if (!isset($configs[$app])) {
                throw new Exception('Application "' . $app . '" does not exists!');
            }
            $config = $configs[$app];

            if (!isset($config['defaultDomain'])) {
                throw new Exception('Application "' . $app . '" has not configured the default domain!');
            }
            $base = $config['defaultDomain'];
            if (strpos($base, 'http') !== 0) {
                $base = 'http://' . $base;
            }
            if (substr($base, -1) != '/') {
                $base .= '/';
            }
            $arr = Yaml::parseFile(ST_CONFIGDIR . '/router.config.yml');
            $urls = $arr['urls'];

            unset($arr);
        }
        return ['base' => $base, 'urls' => $urls];
    }

    public function translateFromUrl($url, $method = null, $app = null, $uri = null) {
        $_method = strtolower($method);

        $params = array();
        foreach ($this->urls as $patternUrl => $methods) {
            if (isset($methods[$_method]) && preg_match("%^$patternUrl$%", $url, $params)) {
                $action = is_array($methods[$_method]) && isset($methods[$_method]['action']) ? $methods[$_method]['action'] : $methods[$_method];
                $paramsName = array_fill_keys(preg_grep('/^[0-9]+$/', array_keys($params), PREG_GREP_INVERT), 0);
                $paramList = array_merge(array_intersect_key($params, $paramsName), Request::getAjaxParams());

                $cache = is_array($methods[$_method]) && isset($methods[$_method]['cache']) ? $methods[$_method]['cache'] : false;

                return new ControllerViewArgs($this->config['controller'], $action, $paramList, $this->config['theme'], '', $cache);
            }
        }

        http_response_code(404);
        throw new \Exception('Nie obsługiwane zapytanie!');
    }

    public function translateToUrl(ControllerViewArgs $controllerViewArgs) {
        return '';
    }

}
