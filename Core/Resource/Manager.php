<?php

namespace Engine5\Core\Resource;

/**
 * Description of Manager
 *
 * @author barcis
 */
final class Manager {

    const RESOURCE_CSS = 'css';
    const RESOURCE_APP = 'app';
    const RESOURCE_JS = 'js';
    const RESOURCE_BOWERCSS = 'bower/css';
    const RESOURCE_BOWERJS = 'bower/js';

    private $resources = array();
    private $js_code = '';
    private $region = null;
    private $resourceCompression = false;
    private $gitStamp = '';

    /**
     * @var Manager
     */
    private static $instance;

    /**
     * @throws \Exception
     */
    public static function create() {
        if (!is_null(self::$instance)) {
            throw new \Exception('recreate ' . __CLASS__);
        }
        self::$instance = new Manager();
    }

    /**
     *
     * @param string $regionName
     * @return Manager
     * @throws \Exception
     */
    public static function getInstance($regionName = null) {
        if (is_null(self::$instance)) {
            throw new \Exception('nie utworzono jeszcze resourcesManagera');
        }

        self::$instance->setRegion($regionName);
        return self::$instance;
    }

    private function __construct() {
        $this->gitStamp = shell_exec("git log -1 --pretty=format:'%h'");

        $this->resources[self::RESOURCE_JS] = array();
        $this->resources[self::RESOURCE_CSS] = array();
        $this->resources[self::RESOURCE_APP] = array();
        $this->resources[self::RESOURCE_BOWERCSS] = array();
        $this->resources[self::RESOURCE_BOWERJS] = array();
    }

    private function setRegion($regionName) {
        $this->region = $regionName;
    }

    final public function setResourceCompression($value) {
        $this->resourceCompression = $value;
    }

    final public function getResourceCompression() {
        return $this->resourceCompression;
    }

    public function add($type, $name, $priority) {
        if (is_null($this->region) || $this->region == '') {
            throw new Exception('próba dodania zasobu (JS lub CSS) poza regionem!');
        }

        if (!in_array($type, self::getResourceTypes())) {
            throw new \Exception('typ zasobu musi być js lub css');
        }

        if (!isset($this->resources[$type][$name])) {
            $this->resources[$type][$name] = array('regions' => array(), 'priority' => -100);
        }

        $this->resources[$type][$name]['regions'][$this->region] = 1;

        if ($this->resources[$type][$name]['priority'] < $priority) {
            $this->resources[$type][$name]['priority'] = $priority;
        }
    }

    public static function getResourceTypes() {
        $resources = [];
        $resources[] = self::RESOURCE_JS;
        $resources[] = self::RESOURCE_CSS;
        $resources[] = self::RESOURCE_APP;
        $resources[] = self::RESOURCE_BOWERCSS;
        $resources[] = self::RESOURCE_BOWERJS;


        return array_keys($resources);
    }

    public function add_js_code($code) {
        if (is_null($this->region) || $this->region == '') {
            throw new \Exception('próba dodania koduy JS poza regionem!');
        }

        $this->js_code .= $code;
    }

    public function get($type) {
        if (!is_null($this->region)) {
            throw new \Exception('próba rendera zasobów (JS lub CSS) w niedozwolonym kontekście!');
        }

        if (!in_array($type, self::getResourceTypes())) {
            throw new \Exception('typ zasobu musi być js lub css');
        }

        uasort($this->resources[$type], array('self', "compare"));

        return $this->resources[$type];
    }

    private static function compare($item1, $item2) {
        if ($item1['priority'] == $item2['priority']) {
            return 0;
        }
        return ($item1['priority'] < $item2['priority']) ? -1 : 1;
    }

    public static function renderJsCode() {
        $code = '';
        if (!empty(self::$instance->js_code)) {
            $code .='<script type=\'text/javascript\'>' . "\n" . self::$instance->js_code . '</script>';
        }
        return $code;
    }

    public static function renderAll($type, $app, $skin) {
        $instance = self::getInstance();
        $code = '';
        $rand = $instance->gitStamp;
        $res = $instance->get($type);
        if ($instance->getResourceCompression()) {
            if ($type == self::RESOURCE_CSS) {
                $subdir = '/css/';
                $ext = 'css';
                $code = "<link rel='stylesheet' media='all' href='/css/_all_combined.css?rand={$rand}' />\n";
            } elseif ($type == self::RESOURCE_BOWERCSS) {
                $subdir = '/plugins/bower/';
                $ext = 'css';
                $code = "<link rel='stylesheet' media='all' href='/plugins/bower/_all_combined.css?rand={$rand}' />\n";
            } elseif ($type == self::RESOURCE_JS) {
                $ext = 'js';
                $subdir = '/';
                $code = "<script type='text/javascript' src='/js/_all_combined.js?rand={$rand}'></script>\n";
            } elseif ($type == self::RESOURCE_APP) {
                $ext = 'js';
                $subdir = '/app/';
                $code = "<script type='text/javascript' src='/app/_all_combined.js?rand={$rand}'></script>\n";
            } elseif ($type == self::RESOURCE_BOWERJS) {
                $ext = 'js';
                $subdir = '/plugins/bower/';
                $code = "<script type='text/javascript' src='/plugins/bower/_all_combined.js?rand={$rand}'></script>\n";
            }
            $fulldir = ST_FRONTENDDIR . $subdir;

            $combinedFile = [];

            foreach (array_keys($res) as $value) {
                $filename = $fulldir . $value . '.' . $ext;
                if (file_exists($filename)) {
                    $combinedFile[$filename] = "\n//---{$filename}---\n" . file_get_contents($filename);
                } else {
                    dd('brak podanego plik resources!!!: ' . $filename);
                }
            }

            file_put_contents($fulldir . '_all_combined.' . $ext, implode('', $combinedFile));
        } else {
            foreach (array_keys($res) as $value) {
                $code .= self::render($type, $app, $skin, $value);
            }
        }

        if ($type === 'app') {
            $code .= "<script type='text/javascript'>
                app
        .value('Engine5'," . json_encode(\Engine5\Core\Engine::getConfig()->configs) . ");
            </script>";
        }


        return $code;
    }

    public static function render($type, $app, $skin, $value) {
        if ($type == self::RESOURCE_CSS) {
            return self::renderCss($app, $skin, $value);
        } elseif ($type == self::RESOURCE_JS) {
            return self::renderJs($app, $skin, $value);
        } elseif ($type == self::RESOURCE_APP) {
            return self::renderApp($app, $value);
        }
    }

    private static function renderCss($app, $skin, $value) {
        $instance = self::getInstance();
        $rand = $instance->gitStamp;
        return "<link rel='stylesheet' media='all' href='/css/{$value}.css?rand={$rand}' />\n";
    }

    private static function renderJs($app, $skin, $value) {
        $instance = self::getInstance();
        $rand = $instance->gitStamp;
        return "<script type='text/javascript' src='/{$value}.js?rand={$rand}'></script>\n";
    }

    private static function renderApp($app, $value) {
        $instance = self::getInstance();
        $rand = $instance->gitStamp;
        return "<script type='text/javascript' src='/app/{$value}.js?rand={$rand}'></script>\n";
    }

}
